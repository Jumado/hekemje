class Product < ActiveRecord::Base
  PRODUCT_TYPES=["Gents","Ladies","Children"]
  validates :item_type,inclusion: PRODUCT_TYPES
  validates_uniqueness_of :name
  validates :category ,:presence=>true
  validates :description ,:presence =>true
  validates :name ,:presence => true
   validates :size ,:presence => true
   validates :image,:presence=> true
   validates :price,:numericality => {:greater_than_or_equal_to =>0.01}
 validates :image,:format => { 
    :with => %r{\.(gif|jpg|png|jpeg)$}i,:multiline =>true,
    :message => 'must be a URL for GIF ,JPG or PNG image.'
    }
  mount_uploader :image ,DesignsUploader
end
