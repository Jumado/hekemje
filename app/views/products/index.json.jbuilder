json.array!(@products) do |product|
  json.extract! product, :id, :name, :description, :size, :featured, :image_url
  json.url product_url(product, format: :json)
end
