class RenameImageUrlToImage < ActiveRecord::Migration
  def change
    change_table(:products) do |t|
      t.rename(:image_url,:image)
    end
  end
end
